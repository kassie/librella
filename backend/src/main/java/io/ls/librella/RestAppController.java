package io.ls.librella;

import io.ls.librella.user.User;
import io.ls.librella.user.UserRepository;
import lombok.extern.slf4j.Slf4j;
import lombok.val;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
final class RestAppController {

    @Autowired
    private UserRepository userRepository;

    @CrossOrigin
    @RequestMapping("/rest")
    User user() {
        val email = "a@a.a";
        return userRepository.findOne(email);
    }

}
