package io.ls.librella;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Component
@ConfigurationProperties(prefix = "test")
public class TestConf {

    private String text;
    private Integer pool;
    private Connectionss connection;

    public Connectionss getConnection() {
        return connection;
    }

    public void setConnection(Connectionss connection) {
        this.connection = connection;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Integer getPool() {
        return pool;
    }

    public void setPool(Integer pool) {
        this.pool = pool;
    }

    public static class Connectionss {

        private Integer max;

        public Integer getMax() {
            return max;
        }

        public void setMax(Integer max) {
            this.max = max;
        }
    }
}
