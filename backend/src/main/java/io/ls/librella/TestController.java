package io.ls.librella;

import com.mongodb.Mongo;
import io.ls.librella.author.Author;
import io.ls.librella.book.Book;
import io.ls.librella.note.Note;
import io.ls.librella.section.Section;
import io.ls.librella.user.User;
import io.ls.librella.user.UserRepository;
import lombok.val;
import org.bson.types.ObjectId;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.*;
import java.util.function.BiFunction;
import java.util.stream.Stream;

import static org.springframework.data.mongodb.core.query.Criteria.where;

@RestController
public class TestController {

    private static final Logger log = LoggerFactory.getLogger(TestController.class);

    @Autowired
    private TestConf cnf;

    @Autowired
    private Mongo mongo;

    @Autowired
    private MongoOperations mongoOperations;

    @Autowired
    private UserRepository userRepository;

    @RequestMapping("/")
    String index() {
        log.debug("Debug controllers");
        return "Hello " + cnf.getText() + cnf.getPool() + " and connection " + cnf.getConnection().getMax();
    }

    @RequestMapping("/ps")
    String ps() {
        if (true) throw new RuntimeException("E");
        return "Text";
    }

    @RequestMapping("/mongo")
    String mongo() {
        log.info("<<<<<<<<<< \n \n <<<<<<<<<<<<<<< \n\n NEW REQUEST");
        mongoOperations.dropCollection("book");
        mongoOperations.dropCollection("author");

        Book b1 = new Book();
        mongoOperations.insert(b1);
        log.debug("b1 id: " + b1.getId());
        Book b2 = new Book();
        mongoOperations.insert(b2);

        Author author = new Author();
        author.setName("A1");
        author.setBooks(Arrays.asList(b1, b2));
        mongoOperations.insert(author);

//        List<Book> books = mongoOperations.find(new Query(where("title").ne("Dor")), Book.class);

        return "ok";
    }

    @RequestMapping("/qr")
    public String queryMongo() {
        List<Author> authors = mongoOperations.find(new Query(where("name").is("A1")), Author.class);
        for (Author a : authors) {
            log.debug("a: " + a);
            for (Book b : a.getBooks()) {
                log.debug("b: " + b);
            }
        }
        return "ok";
    }

    @RequestMapping("/mrepo")
    public String builder() {
        String email = "a@a.a";
        userRepository.delete(email);

        val user = new User();
        user.setEmail(email);
        user.setPassword("A8GY478LZUDSHFGZ743W9I78");

        val section = new Section();
        section.setName("fic");
        section.setId(incId(user.getEmail()));

        val section2 = new Section();
        section2.setName("dev");
        section2.setId(incId(user.getEmail()));
        section2.setAuthors(new ArrayList<>());

        val author1 = new Author();
        author1.setId(incId(section.getId()));
        author1.setName("auth1");
        val author2 = new Author();
        author2.setId(incId(section.getId()));
        author2.setName("auth2");

        val book1 = new Book();
        book1.setId(incId(author1.getId()));
        book1.setTitle("book1");
        book1.setStatus(Book.Status.DEFAULT);
        val book2 = new Book();
        book2.setId(incId(author2.getId()));
        book2.setTitle("book2");
        book2.setStatus(Book.Status.ACTIVE);

        val note = new Note();
        note.setText("note1");
        note.setId(incId(book1.getId()));
        val note2 = new Note();
        note2.setId(incId(book2.getId()));
        note2.setText("note2");

        book1.setNotes(Arrays.asList(note));
        book2.setNotes(Arrays.asList(note2));

        author1.setBooks(Arrays.asList(book1));
        author2.setBooks(Arrays.asList(book2));

        section.setAuthors(Arrays.asList(author1, author2));

        user.setLibrary(Arrays.asList(section, section2));

        userRepository.save(user);

        return "OK";
    }

    private String incId(String base) {
        return base + ";" + new ObjectId().toHexString();
    }

    private void any() {
        val bookid = "asd";
        val user = new User();


        user.getLibrary().forEach(
                section -> section.getAuthors().forEach(
                        author -> author.getBooks().stream().filter(
                                book -> book.getId().equals(bookid)
                        ).findFirst().get()
                )
        );

        List<Section> sections = null;

        find(
                user.getLibrary().stream(),
                (Section section, String id) -> {
                    return find(
                            section.getAuthors().stream(),
                            this::findBookOfAuthor2,
                            bookid
                    );
                },
                bookid
        );
    }

    public static class Container<T> {
        private T item;

        public void set(T item) {
            this.item = item;
        }

        public T get() {
            return item;
        }

        public boolean isPresent() {
            return item != null;
        }
    }

    /**
     * @param <T> type of nested result
     * @param <V> type of collection
     * @param <R> type of finder argument
     */
    private <T, V, R> Container<T> find(Stream<V> stream, BiFunction<V, R, Container<T>> action, R classifier) {
        return stream.reduce(
                new Container<T>(),
                (Container<T> res, V item) -> {
                    Container<T> temp = action.apply(item, classifier);
                    if (temp.isPresent()) {
                        res.set(temp.get());
                    }
                    return res;
                },
                (Container<T> i1, Container<T> i2) -> {
                    if (i1.isPresent()) {
                        return i1;
                    } else {
                        return i2;
                    }
                }
        );
    }

//    private Optional<Book> findBookOfUser(User user, String id) {
//        return user.getLibrary().stream().reduce(
//                new HashSet<Book>(),
//                (HashSet<Book> res, Section section) -> {
//                    val books = findBookOfSection(section.getAuthors(), id);
//                    res.addAll(books);
//                    return res;
//                },
//                (HashSet<Book> b1, HashSet<Book> b2) -> {
//                    b1.addAll(b2);
//                    return b1;
//                }
//        );
//    }

    private HashSet<Book> findBookOfSection(Collection<Author> authors, String id) {
        return authors.stream().reduce(
                new HashSet<Book>(),
                (res, author) -> {
                    Optional<Book> bookOfAuthor = findBookOfAuthor(author, id);
                    if (bookOfAuthor.isPresent()) {
                        res.add(bookOfAuthor.get());
                    }
                    return res;
                },
                (b1, b2) -> {
                    b1.addAll(b2);
                    return b1;
                }

        );
    }

    private Optional<Book> findBookOfAuthor(Author author, String id) {
        return author.getBooks().stream().filter(book -> id.equals(book.getId())).findFirst();
    }

    private Container<Book> findBookOfAuthor2(Author author, String id) {
        val cont = new Container<Book>();
        val opt = author.getBooks().stream().filter(book -> id.equals(book.getId())).findFirst();
        if (opt.isPresent()) {
            cont.set(opt.get());
        }
        return cont;
    }

}
