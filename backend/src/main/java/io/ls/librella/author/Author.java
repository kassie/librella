package io.ls.librella.author;

import io.ls.librella.book.Book;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class Author {

    private String id;
    private String name;
    private List<Book> books;

}
