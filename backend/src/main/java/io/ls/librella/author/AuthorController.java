package io.ls.librella.author;

import io.ls.librella.book.Book;
import io.ls.librella.book.BookService;
import lombok.extern.slf4j.Slf4j;
import lombok.val;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@Slf4j
@CrossOrigin
@RestController
final class AuthorController {

    @Autowired
    private BookService bookService;

    @Autowired
    private AuthorService authorService;

    @RequestMapping(value = "/author/addBook", method = RequestMethod.POST)
        String addBook(@RequestParam String id, @RequestParam String title) {
        val book = bookService.createBook(title);
        authorService.addBook(id, book);
        return book.getId();
    }
}
