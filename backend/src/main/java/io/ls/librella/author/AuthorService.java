package io.ls.librella.author;

import io.ls.librella.book.Book;

public interface AuthorService {

    void addBook(String authorId, Book book);
}
