package io.ls.librella.author;

import io.ls.librella.book.Book;
import io.ls.librella.id.IdPath;
import io.ls.librella.library.LibraryRootAware;
import lombok.val;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
final class DefaultAuthorService implements AuthorService {

    @Autowired
    private LibraryRootAware libraryRoot;

    @Override
    public void addBook(String authorId, Book book) {
        val idPath = IdPath.of(authorId);
        book.setId(idPath.incId());
        libraryRoot.withAuthor(idPath, author -> {
            author.getBooks().add(book);
        });
    }
}
