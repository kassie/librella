package io.ls.librella.book;

import io.ls.librella.note.Note;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@ToString
public class Book {

    private String id;
    private String title;
    private List<Note> notes = new ArrayList<>();
    private Status status = Status.DEFAULT;

    public enum Status {
        DEFAULT, ACTIVE, DONE, PENDING, REJECTED
    }

    public boolean valid() {
        return validTitle() && validStatus();
    }

    private boolean validTitle() {
        return title != null && title.trim().length() > 0;
    }

    private boolean validStatus() {
        return status != null;
    }

}
