package io.ls.librella.book;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@Slf4j
@CrossOrigin
@RestController
final class BookController {

    @Autowired
    private BookService bookService;

    @RequestMapping(value = "/book/update", method = RequestMethod.POST)
    void updateBook(@RequestParam String id, @RequestParam(required = false) String title, @RequestParam(required = false) Book.Status status) {
        log.debug(String.format("Book update id %s, title %s, status %s", id, title, status == null ? null : status.toString()));
        bookService.updateBook(id, title, status);
    }

}
