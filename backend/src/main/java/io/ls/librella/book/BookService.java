package io.ls.librella.book;

public interface BookService {

    void updateBook(String id, String title, Book.Status status);

    Book createBook(String title);
}
