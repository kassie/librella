package io.ls.librella.book;

import io.ls.librella.id.IdPath;
import io.ls.librella.library.LibraryException;
import io.ls.librella.library.LibraryRootAware;
import lombok.val;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
final class DefaultBookService implements BookService {

    @Autowired
    private LibraryRootAware library;

    @Override
    public void updateBook(String id, String title, Book.Status status) {
        library.withBook(IdPath.of(id), persistedBook -> {
            updateBook(persistedBook, title, status);
        });
    }

    @Override
    public Book createBook(String title) {
        val newBook = new Book();
        newBook.setTitle(title);
        if(!newBook.valid()) {
            throw new LibraryException(LibraryException.Type.INVALID_ENTITY, "Attempt to create invalid book");
        }
        return newBook;
    }

    private void updateBook(Book book, String title, Book.Status status) {
        if (title != null) {
            book.setTitle(title);
        }
        if (status != null) {
            book.setStatus(status);
        }
    }
}
