package io.ls.librella.id;

import io.ls.librella.author.Author;
import io.ls.librella.book.Book;
import io.ls.librella.note.Note;
import io.ls.librella.section.Section;
import io.ls.librella.user.User;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.val;
import org.bson.types.ObjectId;

import java.util.Arrays;

@Getter
@EqualsAndHashCode
public final class IdPath {

    private static final String DELIMITER = ";";
    public static final int ID_MAX_LENGTH = 5;

    private final String user;
    private final String section;
    private final String author;
    private final String book;
    private final String note;

    private IdPath(String... ids) {
        val padded = Arrays.copyOf(ids, ID_MAX_LENGTH);
        this.user = padded[0];
        this.section = padded[1] == null ? null : user + DELIMITER + padded[1];
        this.author = padded[2] == null ? null : section + DELIMITER + padded[2];
        this.book = padded[3] == null ? null : author + DELIMITER + padded[3];
        this.note = padded[4] == null ? null : book + DELIMITER + padded[4];
    }

    public static IdPath of(User user) {
        return initFromSplit(user.getEmail());
    }

    public static IdPath of(Section section) {
        return initFromSplit(section.getId());
    }

    public static IdPath of(Author author) {
        return initFromSplit(author.getId());
    }

    public static IdPath of(Book book) {
        return initFromSplit(book.getId());
    }

    public static IdPath of(Note note) {
        return initFromSplit(note.getId());
    }

    public static IdPath of(String id) {
        return initFromSplit(id);
    }

    private static IdPath initFromSplit(String id) {
        return new IdPath(parseId(id));
    }

    private static String[] parseId(String id) {
        return id.split(DELIMITER);
    }

    public String incId() {
        return toStringId() + DELIMITER + new ObjectId().toHexString();
    }

    public String toStringId() {
        return note != null ? note : book != null ? book : author != null ? author : section != null ? section : user;
    }

    @Override
    public String toString() {
        return this.toStringId();
    }
}
