package io.ls.librella.library;

import lombok.Getter;

public class LibraryException extends RuntimeException {

    @Getter
    private final Type type;

    public LibraryException(Type type, String message) {
        super(message);
        this.type = type;
    }

    public enum Type {
        INVALID_ENTITY
    }
}
