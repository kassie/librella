package io.ls.librella.library;

import io.ls.librella.author.Author;
import io.ls.librella.book.Book;
import io.ls.librella.id.IdPath;
import io.ls.librella.section.Section;
import io.ls.librella.user.User;
import io.ls.librella.user.UserRepository;
import lombok.val;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.function.Consumer;

@Service
public final class LibraryRoot implements LibraryRootAware {

    @Autowired
    private UserRepository userRepository;

    @Override
    public void withBook(IdPath idPath, Consumer<Book> function) {
        withUser(idPath, user -> {
            function.accept(findBookInRoot(idPath, user));
        });
    }

    @Override
    public void withAuthor(IdPath idPath, Consumer<Author> action) {
        withUser(idPath, user -> {
            action.accept(findAuthorInRoot(idPath, user));
        });
    }

    private void withUser(IdPath idPath, Consumer<User> action) {
        val user = userRepository.findOne(idPath.getUser());
        action.accept(user);
        userRepository.save(user);
    }

    private Book findBookInRoot(IdPath idPath, User user) {
        return findBook(idPath, findAuthor(idPath, findSection(idPath, user)));
    }

    private Author findAuthorInRoot(IdPath idPath, User user) {
        return findAuthor(idPath, findSection(idPath, user));
    }

    private Book findBook(IdPath idPath, Author author) {
        return author.getBooks().stream().filter(book -> idPath.getBook().equals(book.getId())).findFirst().get();
    }

    private Section findSection(IdPath idPath, User user) {
        return user.getLibrary().stream().filter(section -> idPath.getSection().equals(section.getId())).findFirst().get();
    }

    private Author findAuthor(IdPath idPath, Section section) {
        return section.getAuthors().stream().filter(author -> idPath.getAuthor().equals(author.getId())).findFirst().get();
    }

}
