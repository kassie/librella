package io.ls.librella.library;

import io.ls.librella.author.Author;
import io.ls.librella.book.Book;
import io.ls.librella.id.IdPath;

import java.util.function.Consumer;

public interface LibraryRootAware {

    void withBook(IdPath idPath, Consumer<Book> action);

    void withAuthor(IdPath idPath, Consumer<Author> author);
}
