package io.ls.librella.note;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Note {

    private String id;
    private String text;
}
