package io.ls.librella.section;

import io.ls.librella.author.Author;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class Section {

    private String id;
    private String name;
    private List<Author> authors;
}
