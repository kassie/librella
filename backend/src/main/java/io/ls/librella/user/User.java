package io.ls.librella.user;

import io.ls.librella.section.Section;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;

@Getter
@Setter
@Document
public class User {

    @Id
    private String email;
    private String password;
    private List<Section> library;
}
