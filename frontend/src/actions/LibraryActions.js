import library from '../api/library'
import * as types from '../constants/ActionTypes'

let lastBookId = 0
let lastAuthorId = 0
function receiveBooks(library) {
	lastBookId = lastKey(library.entities.books)
	lastAuthorId = lastKey(library.entities.authors)
	return {
		type: types.RECEIVE_BOOKS,
		payload: library
	}
}

function lastKey(obj) {
	const keys = Object.keys(obj)
	return keys[keys.length - 1]
}

export function addSection(name) {
	return {
		type: types.ADD_SECTION,
		payload: {
			name: name
		}
	}
}

export function addBook(authorId, name) {
	return {
		type: types.ADD_BOOK,
		payload: {
			id: ++lastBookId,
			authorId: authorId,
			name: name
		}
	}
}

export function removeBook(bookId, authorId) {
	return {
		type: types.REMOVE_BOOK,
		payload: {
			bookId: bookId,
			authorId: authorId
		}
	}
}

export function search(query, type) {
	return {
		type: types.SEARCH,
		payload: {
			query: query,
			type: type
		}
	}
}

export function addAuthor(sectionId, name) {
	return {
		type: types.ADD_AUTHOR,
		payload: {
			id: ++lastAuthorId,
			sectionId: sectionId,
			name: name
		}
	}
}

export function getAllBooks() {
	return dispatch => {
		library.getBooks(books => {
			dispatch(receiveBooks(books))
		})
	}
}

export function updateStatus(bookId, status) {
	return {
		type: types.UPDATE_STATUS,
		payload: {
			book: bookId,
			status: status
		}
	}
}