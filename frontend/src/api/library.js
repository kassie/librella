import _library from './library.json'
import { normalize, Schema, arrayOf } from 'normalizr';

export default {
    getBooks(cb) {
        cb(gatherLibrary())
    }
}

const section = new Schema('sections', {
	idAttribute: sec => sec.name.toLowerCase()
})
const author = new Schema('authors')
const book = new Schema('books')

section.define({
	authors: arrayOf(author)
})

author.define({
	books: arrayOf(book)
})

function gatherLibrary() {
	return normalize(_library, {library: arrayOf(section) })
}
