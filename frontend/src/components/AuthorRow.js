import React, { PropTypes, Component } from 'react'
import BookEntry from '../containers/BookEntry'

export default class AuthorRow extends Component {
	constructor(props) {
		super(props)
		this.state = {
			showAddBook: false,
			newBookName: ''
		}
		this.handleAddClick = this.handleAddClick.bind(this)
		this.handleCancelClick = this.handleCancelClick.bind(this)
		this.handleEnter = this.handleEnter.bind(this)
		this.handleChange = this.handleChange.bind(this)
		this.handleRemoveBook = this.handleRemoveBook.bind(this)
		this.handleBlur = this.handleBlur.bind(this)
	}
	handleChange(e) {
		this.setState({newBookName: e.target.value})
	}
	handleAddClick() {
		this.setState({showAddBook: true})
	}
	hideAddFields() {
		this.setState({showAddBook: false})
	}
	handleCancelClick() {
		this.hideAddFields()
	}
	handleEnter(e) {
		if(e.key === 'Enter') {
			this.props.addBook(this.props.author.id, this.state.newBookName)
			this.hideAddFields()
		} else if (e.key === 'Escape') {
			this.hideAddFields()
		}
	}
	handleRemoveBook(bookId) {
		this.props.removeBook(bookId, this.props.author.id)
	}
	handleBlur() {
		this.hideAddFields()
	}
	renderAddBook() {
		if(this.state.showAddBook) {
        	return (
	    		<li className='add-book-input'>
	        		<input onChange={this.handleChange} onBlur={this.handleBlur} onKeyDown={this.handleEnter} ref={(it) =>  { if(it !== null) it.focus() }} type='text' value={this.state.value} />
	        		<a href='#' onClick={this.handleCancelClick} className='cancel-button tile tile-danger'>Cancel</a>
	    		</li>
    		)
    	} else {	 
    		return <li><a onClick={this.handleAddClick} href='#' className='tile tile-success add-book'>+</a></li>
    	}
	}
	render() {
		const { author, addBook, query, filter } = this.props
		return(
			<div className='row author-row'>
	            <div className='col-md-2 col-xs-4'>
	              <input className='author author-label' type='text' defaultValue={author.name}/>
	            </div>
	            <div className='col-md-10 col-xs-8'>
		            <ul className='nav nav-pills cells'>
		            	{author.books.filter((it) => {return !filter.enabled || filter.result.includes(it.toString())}).map((bookId) => {
		            		return <BookEntry key={bookId} id={bookId} remove={this.handleRemoveBook} />
		            	})}
	           			{this.renderAddBook()}
		            </ul>
	            </div>
	        </div>
		)
	}
}

AuthorRow.propTypes = {
	author: PropTypes.object.isRequired,
	addBook: PropTypes.func.isRequired,
	filter: PropTypes.object.isRequired,
	removeBook: PropTypes.func.isRequired
}