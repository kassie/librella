import React, { PropTypes, Component } from 'react'
import * as statuses from '../constants/BookStatuses'

const BOOK_STATUS_CLASSES = {
	[statuses.DEFAULT]: 'tile-default',
	[statuses.DONE]: 'tile-success',
	[statuses.ACTIVE]: 'tile-warning',
	[statuses.REJECTED]: 'tile-danger',
	[statuses.PENDING]: 'tile-info'
}

export default class Book extends Component  {
	constructor(props) {
		super(props)
		this.state = {
			showConfirmIcons: false
		}
		this.handleRemoveClick = this.handleRemoveClick.bind(this)
		this.handleRemoveConfirmClick = this.handleRemoveConfirmClick.bind(this)
		this.handleRemoveRejectClick = this.handleRemoveRejectClick.bind(this)
		this.handleRemoveLinkMouseOut = this.handleRemoveLinkMouseOut.bind(this)
	}

	handleRemoveClick() {
		console.log('remove')
		this.setState({showConfirmIcons: true})
	}

	handleRemoveConfirmClick() {
		this.props.remove(this.props.book.id)
	}

	handleRemoveRejectClick() {
		console.log('reject')
		this.setState({showConfirmIcons: false})
	}

	handleRemoveLinkMouseOut() {
		this.setState({showConfirmIcons: false})
	}

	render() {
		const { book, updateStatus, remove } = this.props
		return(
			<li className='dropdown '>
	          <a href='#' className={`tile ${BOOK_STATUS_CLASSES[book.status]}`}>{book.name}</a>
	          <ul className='dropdown-menu book-menu' >
	            <li><a href='#' className={`tile ${BOOK_STATUS_CLASSES[statuses.ACTIVE]}`} onClick={() => updateStatus(book.id, statuses.ACTIVE)} >In progress</a></li>
	            <li><a href='#' className={`tile ${BOOK_STATUS_CLASSES[statuses.DONE]}`} onClick={() => updateStatus(book.id, statuses.DONE)} >Done</a></li>
	            <li><a href='#' className={`tile ${BOOK_STATUS_CLASSES[statuses.REJECTED]}`} onClick={() => updateStatus(book.id, statuses.REJECTED)}>Rejected</a></li>
	            <li><a href='#' className={`tile ${BOOK_STATUS_CLASSES[statuses.PENDING]}`} onClick={() => updateStatus(book.id, statuses.PENDING)}>Pending</a></li>
	            <li><a href='#' className={`tile ${BOOK_STATUS_CLASSES[statuses.DEFAULT]}`} onClick={() => updateStatus(book.id, statuses.DEFAULT)}>Default</a></li>
	            <li >
	            	<a href='#' className={`relative tile ${BOOK_STATUS_CLASSES[statuses.DEFAULT]}`}>
	            		<span onClick={this.handleRemoveClick}>Remove</span>
	            		<span className={`confirm-icons ${this.state.showConfirmIcons ? '' : 'hidden'}`}>
	            			<span onClick={this.handleRemoveConfirmClick} className='glyphicon glyphicon-ok icon-confirm' aria-hidden='true'></span>
	            			<span onClick={this.handleRemoveRejectClick} className='glyphicon glyphicon-remove icon-reject' aria-hidden='true'></span>
	        			</span>
            		</a>
        		</li>
	          </ul>
	        </li>
		)
	}
}

Book.propTypes = {
	book: PropTypes.object.isRequired,
	updateStatus: PropTypes.func.isRequired,
	remove: PropTypes.func.isRequired
}

