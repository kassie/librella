import React, {	PropTypes, 	Component } from 'react'
import SearchContainer from '../containers/SearchContainer'

export default class Header extends Component {
	render() {
		return <div className='container-fluid'>
		  <nav id='bs-navbar' className='navbar-collapse'>
		    <ul className='nav navbar-nav'>
		      <li className='active'><a href='/' className='upper-case'>Librella</a></li>
		    </ul>
		    <ul className='nav navbar-nav navbar-right'>
		      <li>
	      		<SearchContainer />
  			  </li>
		    </ul>
		  </nav>
		</div>
	}
}

Header.propTypes = {
}