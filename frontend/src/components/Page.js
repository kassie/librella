import React, {	PropTypes, Component } from 'react'
import AuthorRowContainer from '../containers/AuthorRowContainer'

export default class Page extends Component {
	constructor(props, context) {
		super(props, context)
		this.state = {
			activeSection: this.props.library[0],
			addingAuthor: false,
			newAuthorName: '',
			showAddSection: false,
			newSectionName: ''
		}
		this.handleAddAuthor = this.handleAddAuthor.bind(this)
		this.handleCancelAddAuthor = this.handleCancelAddAuthor.bind(this)
		this.handleChangeNewAuthor = this.handleChangeNewAuthor.bind(this)
		this.handleNewAuthorEnter = this.handleNewAuthorEnter.bind(this)
		this.handleShowAddSection = this.handleShowAddSection.bind(this)
		this.handleAddSection = this.handleAddSection.bind(this)
		this.handleNewSectionNameChange = this.handleNewSectionNameChange.bind(this)
		this.handleNewSectionCancelClick = this.handleNewSectionCancelClick.bind(this)
		this.handleSwitchTab = this.handleSwitchTab.bind(this)
		this.handleAddAuthorInputBlur = this.handleAddAuthorInputBlur.bind(this)
		this.handleAddSectionInputBlur = this.handleAddSectionInputBlur.bind(this)
	}

	handleSwitchTab(section) {
		this.setState({activeSection: section})
	}

	handleAddAuthor() {
		this.setState({addingAuthor: true})
	}

	handleCancelAddAuthor() {
		this.hideNewAuthorInput()
	}

	handleChangeNewAuthor(e) {
		this.setState({newAuthorName: e.target.value})
	}

	handleNewAuthorEnter(e) {
		if(e.key === 'Enter') {
			this.hideNewAuthorInput()	
			this.props.addAuthor(this.state.activeSection, this.state.newAuthorName)
		} else if (e.key === 'Escape') {
			this.hideNewAuthorInput()
		}
	}

	handleAddSectionInputBlur() {
		this.hideNewSectionControls()
	}

	handleAddAuthorInputBlur() {
		this.hideNewAuthorInput()
	}

	hideNewAuthorInput() {
		this.setState({addingAuthor: false, newAuthorName: ''})
	}

	renderSectionHeaders() {
		const { library, sections} = this.props
		return  library.map((sectionId, i) => {
		      		const section = sections[sectionId]
		        	return(
		        		<li role='presentation' key={i} className={this.state.activeSection == sectionId ? 'active' : ''}>
		        			<a key={i} href={`#${section.name}`} onClick={() => this.handleSwitchTab(sectionId)}>
		        				{section.name}
	        				</a>
		        		</li>
	        		)
	      		})
	}

	renderAddAuthor() {
		if(this.state.addingAuthor) {
			return (
				<div>
					<input onKeyDown={this.handleNewAuthorEnter} 
							onBlur={this.handleAddAuthorInputBlur}
							onChange={this.handleChangeNewAuthor} 
							ref={(it) =>  { if(it !== null) it.focus() }} 
							type='text' className='add-author-input'
							value={this.state.newAuthorName} />
					<a onClick={this.handleCancelAddAuthor} href='#' className='btn tile tile-danger'>Cancel</a>
				</div>
			)
		} else {
			return <a onClick={this.handleAddAuthor} href='#' className='btn tile add-author tile-success'>Add author</a>
		}
	}

	handleNewSectionNameChange(e) {
		this.setState({newSectionName: e.target.value})
	}

	handleShowAddSection() {
		this.setState({showAddSection: true})
	}

	handleAddSection(e) {
		if(e.key === 'Enter' && this.state.newSectionName.length > 0) {
			this.props.addSection(this.state.newSectionName)
			this.hideNewSectionControls()
		} else if (e.key === 'Escape') {
			this.hideNewSectionControls()
		}
	}

	handleNewSectionCancelClick() {
		this.hideNewSectionControls()
	}

	hideNewSectionControls() {
		this.setState({showAddSection: false, newSectionName: ''})
	}

	renderAddSection() {
		let inner
		if(this.state.showAddSection) {
			inner = 
				<span className='new-section-input'>
					<input type='text' 
						onKeyDown={this.handleAddSection} 
						onBlur={this.handleAddSectionInputBlur}
						onChange={this.handleNewSectionNameChange}  
						ref={(it) => { if(it !== null) it.focus()} } 
						value={this.state.newSectionName} />
					<a href='#' onClick={this.handleNewSectionCancelClick} className='cancel-button tile tile-danger'>Cancel</a>
				</span>
		} else {
			inner = <a href='#' onClick={this.handleShowAddSection} >+</a>
		}

		return (
			<li className={`fade ${this.state.showAddSection ? 'in' : ''}`}>
				{inner}
			</li>
		)
	}

	render() {
		const { library, sections, authors, addBook } = this.props

		return (
			<div className='container-fluid pale'>
			  <div className='row'>
			    <div className='col-md-12'>
			      <ul className='nav nav-tabs' role='tablist'>
			      	{this.renderSectionHeaders()}
			      	{this.renderAddSection()}
			      </ul>

			      <div className='tab-content'>
			      	{library.map((sectionId, i) => {
			      		const section = sections[sectionId]
			        	return(
			        		<div key={i} role='tabpanel' className={`tab-pane fade ${this.state.activeSection == sectionId ? 'in active' : ''}`} id={section.name}>
								{section.authors.map((authorId) => {
									return <AuthorRowContainer key={authorId} id={authorId} />
								})}
								<div className='row'>
									<div className='col-md-4'>
										{this.renderAddAuthor()}
									</div>
								</div>
	        				</div>
		        		)	 
			      	})}
			      </div>
			    </div>
			  </div>
			</div>
		)
	}
}

Page.propTypes = {
	sections: PropTypes.object.isRequired,
	authors: PropTypes.object.isRequired,
	library: PropTypes.array.isRequired,
	addBook: PropTypes.func.isRequired,
	addAuthor: PropTypes.func.isRequired,
	addSection: PropTypes.func.isRequired
}