import React, {	PropTypes, 	Component } from 'react'
import * as searchTypes from '../constants/SearchTypes'

export default class Search extends Component {
	constructor(props) {
		super(props)
		this.state = {
			type: searchTypes.BOOK,
			query: ''
		}
		this.handleQueryChange = this.handleQueryChange.bind(this)
	}

	handleQueryChange(e) {
		this.setState({query: e.target.value})
		this.props.search(e.target.value, this.state.type)
	}

	render() {
		const placeholder = `Search ${this.state.type}s...`

		return(
			<input 
	      		onChange={this.handleQueryChange}
	      		className='form-control' style={{margin: '9px 0'}} type='text' 
	      		value={this.state.query} placeholder={placeholder} />
		)
	}
}

Search.propTypes = {
	search: PropTypes.func.isRequired
}