import React, { Component} from 'react'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import Page from '../components/Page'
import Header from '../components/Header'
import { addBook, addAuthor, addSection} from '../actions/LibraryActions'

class App extends Component {
	render() {
		const { authors, sections, library, addBook, addAuthor, addSection } = this.props
		return <div>
			<Header />
			<Page library={library} sections={sections} authors={authors} addBook={addBook} addAuthor={addAuthor} addSection={addSection} />
		</div> 
	}
}

function mapStateToProps(state) {
	return {
		...state
	}
}

function mapDispatchToProps(dispatch) {
	return {
		addBook: bindActionCreators(addBook, dispatch),
		addAuthor: bindActionCreators(addAuthor, dispatch),
		addSection: bindActionCreators(addSection, dispatch)
	}
}

export default connect(mapStateToProps, mapDispatchToProps)(App)