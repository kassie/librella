import { connect } from 'react-redux'
import Book from '../components/Book'
import { updateStatus } from '../actions/LibraryActions'

const mapStateToProps = (state, ownProps) => {
  return {
    book: state.books[ownProps.id],
  }
}

const mapDispatchToProps = (dispatch, ownProps) => {
  return {
    updateStatus: (id, status) => {
      dispatch(updateStatus(id, status))
    }
  }
}


export default connect(mapStateToProps, mapDispatchToProps)(Book)
