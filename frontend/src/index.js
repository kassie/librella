import 'babel-polyfill'
import React from 'react'
import { render } from 'react-dom'
import { Provider } from 'react-redux'
import App from './containers/App'
import configureStore from './store/configureStore'
import { getAllBooks } from './actions/LibraryActions'

const store = configureStore()
store.dispatch(getAllBooks())

render(
	<Provider store={store}>
	  <App />
	</Provider>,
	document.getElementById('root')
)