import { combineReducers } from 'redux'
import * as types from '../constants/ActionTypes'
import * as searchTypes from '../constants/SearchTypes'
import update from 'react-addons-update'
import books from './books'
import authors from './authors'
import { sections, library } from './sections'

export default function app(state = {}, action) {
    return {
        filter: search(state, action),
        books: books(state.books, action),
        authors: authors(state.authors, action),
        sections: sections(state.sections, action),
        library: library(state.library, action)
    }
}

const defaultFilter = {enabled: false, result: []}
function search(state = {filter: defaultFilter}, action) {
	if(action.type != types.SEARCH) {
		return state.filter ? state.filter : defaultFilter
	}

	const { query, type } = action.payload
    console.log(`Searching ${type}s: '${query}'`)
    if(query == null || query.length == 0) {
        return defaultFilter
    }
    switch(type) {
        case searchTypes.BOOK:
            return {enabled: true, result: filterBooks(state, query.toLowerCase()) }
        case searchTypes.AUTHOR:
            return defaultFilter
    }
}

function filterBooks(state, query) {
    const targetKeys = []
    Object.keys(state.books).forEach((it) => {
        if(state.books[it].name.toLowerCase().indexOf(query) >= 0) {
            targetKeys.push(it)
        } 
    }) 

    return targetKeys
}

