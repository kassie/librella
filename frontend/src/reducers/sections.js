import * as types from '../constants/ActionTypes'

export function sections(sections = {}, action) {
    switch(action.type) {
        case types.ADD_AUTHOR: {
            const {id, sectionId} = action.payload
            return {
                ...sections,
                [sectionId]: {
                    ...sections[sectionId],
                    ...{authors: [...sections[sectionId].authors, id]}
                }
            }
        }
        case types.ADD_SECTION: {
            const { name } = action.payload

            const newKey = sectionKey(name)
            if(Object.keys(sections).includes(newKey)) {
                return sections
            } else {
                return {
                    ...sections,
                    [newKey]: {
                        name: name,
                        authors: []
                    }
                }
            }
        }
        default:
        	if(action.payload && action.payload.entities && action.payload.entities.sections) {
        		return {
        			...{},
        			...sections,
        			...action.payload.entities.sections,
        		}
        	}
        	return sections
    }
}

export function library(library = [], action) {
    switch(action.type) {
        case types.RECEIVE_BOOKS: {
            return action.payload.result.library
        }
        case types.ADD_SECTION: {
            const key = sectionKey(action.payload.name)
            if(library.includes(key)) {
                return library
            } else {
                return [...library, key]
            }
        }
        default:
            return library
    }
}

function sectionKey(name) {
    return name.toLowerCase()
}