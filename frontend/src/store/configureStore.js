import { createStore, applyMiddleware } from 'redux'
import rootReducer from '../reducers'
import createLogger from 'redux-logger'
import thunk from 'redux-thunk'

const middleware = process.env.NODE_ENV === 'production' ?
  [ thunk ] :
  [ thunk, createLogger() ]

export default function configureStore(initialState) { 
	const store = createStore(
		rootReducer, 
		initialState,
		applyMiddleware(...middleware)
	)

	if(module.hot) {
		module.hot.accept('../reducers', () => {
			const nextRootReducer = require('../reducers')
			store.replaceReducer(nextRootReducer)
		})
	}

	return store
}

