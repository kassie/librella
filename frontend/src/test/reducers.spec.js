import 'babel-polyfill'
import expect from 'expect'
import reducer from '../reducers/index'
import * as types from '../constants/ActionTypes'
import * as bookStatuses from '../constants/BookStatuses'

function defaultState() {
    return {
        authors: {},
        books: {},
        sections: {},
        filter: {
            enabled: false,
            result: []
        },
        library: []
    }
}

function book(id, name, status = bookStatuses.DEFAULT) {
	return {
		id: id,
		name: name,
		status: status
	}
}

describe('reducer', () => {
    it('should return initial state', () => {
        expect(reducer(undefined, {}))
            .toEqual(defaultState())
    })

    it('should add book to empty list', () => {
        const initialState = defaultState()
        initialState.authors = { 1: { id: 1, name: "name", books: [] } }

        const action = {
            type: types.ADD_BOOK,
            payload: { id:1, authorId: 1, name: "b1" }
        }

        const resultState = defaultState()
        resultState.authors = { 1: { id: 1, name: "name", books: [1] } }
        resultState.books = { 1: { id: 1, name: "b1", status: bookStatuses.DEFAULT } }

        expect(reducer(initialState, action))
            .toEqual(resultState)
    })

    it('should add book to not empty list', () => {
        const initialState = defaultState()
        initialState.authors = { 1: { id: 1, name: "name", books: [1] } }
        initialState.books = { 1: book(1, "b2") }

        const action = {
            type: types.ADD_BOOK,
            payload: { id:2, authorId: 1, name: "b1" }
        }

        const resultState = defaultState()
        resultState.authors = { 1: { id: 1, name: "name", books: [1,2] } }
        resultState.books = { 
        	1: book(1, "b2"),
        	2: book(2, "b1"),
        }

        expect(reducer(initialState, action))
            .toEqual(resultState)
    })

    it('should delete book', () => {
        const initialState = defaultState()
        initialState.authors = { 1: { id: 1, name: "name", books: [1] } }
        initialState.books = { 1: book(1, "b2") }

        const action = {
            type: types.REMOVE_BOOK,
            payload: { bookId:1, authorId: 1 }
        }

        const resultState = defaultState()
        resultState.authors = { 1: { id: 1, name: "name", books: [] } }

        expect(reducer(initialState, action))
            .toEqual(resultState)
    })

    it('should add section to empty list', () => {
        const initialState = defaultState()

        const action = {
            type: types.ADD_SECTION,
            payload: { name: "section"}
        }

        const resultState = defaultState()
        resultState.sections = {section: {name: 'section', authors: []}}
        resultState.library = ['section']

        expect(reducer(initialState, action))
            .toEqual(resultState)
    })

    it('should add section to non empty list', () => {
        const initialState = defaultState()
        initialState.sections = {s1: {name: 's1', authors: []}}
        initialState.library = ['s1']

        const action = {
            type: types.ADD_SECTION,
            payload: { name: "section"}
        }

        const resultState = defaultState()
        resultState.sections = {s1: {name: 's1', authors:[]},section: {name: 'section', authors: []}}
        resultState.library = ['s1', 'section']

        expect(reducer(initialState, action))
            .toEqual(resultState)
    })


})
