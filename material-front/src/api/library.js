// import _library from './library.json'
import { normalize, Schema, arrayOf } from 'normalizr';
import fetch from 'isomorphic-fetch'
import props from '../props.json'
import * as endpoints from './Endpoints'

const section = new Schema('sections')
// , {
// 	idAttribute: sec => sec.name.toLowerCase()
// })
const author = new Schema('authors')
const book = new Schema('books')

section.define({
	authors: arrayOf(author)
})

author.define({
	books: arrayOf(book)
})

function normalized(json) {
	console.log('TO NORMALIZE')
	console.log(json)
	return normalize(json, {library: arrayOf(section) })
}

function gatherLibrary(cb) {
	// cb(normalized(_library))

	console.log('MAKING REQUEST')
	return fetch(props.api.url + endpoints.GET_ALL)
			.then(response => response.json().then(json => ({ json, response })))
			.then(({json, response}) => {
				console.log('RESPONSE')
				console.log(response.ok)
				console.log(json)
				cb(normalized(json))
			})
}

export default {
    getBooks(cb) {
        gatherLibrary(cb)
    }
}
