import React, { PropTypes, Component } from 'react'
import BookEntry from '../containers/BookEntry'
import TextField from 'material-ui/TextField'
import {Row, Col} from 'react-flexbox-grid/lib'
import {grey400} from 'material-ui/styles/colors'
import Snackbar from 'material-ui/Snackbar'

export default class AuthorRow extends Component {
	constructor(props) {
		super(props)
		this.state = {
			showAddBook: false,
			newBookName: '',
			snackBarOpen: false
		}
		this.handleAddClick = this.handleAddClick.bind(this)
		this.handleCancelClick = this.handleCancelClick.bind(this)
		this.handleEnter = this.handleEnter.bind(this)
		this.handleChange = this.handleChange.bind(this)
		this.handleRemoveBook = this.handleRemoveBook.bind(this)
		this.handleBlur = this.handleBlur.bind(this)
	}
	handleChange(e) {
		this.setState({newBookName: e.target.value})
	}
	handleAddClick() {
		this.setState({showAddBook: true})
	}
	hideAddFields() {
		this.setState({showAddBook: false})
	}
	handleCancelClick() {
		this.hideAddFields()
	}
	handleEnter(e) {
		if(e.key === 'Enter') {
			this.props.addBook(this.props.author.id, this.state.newBookName)
			this.hideAddFields()
		} else if (e.key === 'Escape') {
			this.hideAddFields()
		}
	}
	handleRemoveBook(bookId) {
		this.props.removeBook(bookId, this.props.author.id)
		this.setState({snackBarOpen: true})
	}
	handleBlur() {
		this.hideAddFields()
	}
	renderAddBook() {
		if(this.state.showAddBook) {
        	return (
	    		<li className='add-book-input'>
	        		<input onChange={this.handleChange} onBlur={this.handleBlur} onKeyDown={this.handleEnter} ref={(it) =>  { if(it !== null) it.focus() }} type='text' value={this.state.value} />
	        		<a href='#' onClick={this.handleCancelClick} className='cancel-button tile tile-danger'>Cancel</a>
	    		</li>
    		)
    	} else {	 
    		return <li><a onClick={this.handleAddClick} href='#' className='tile tile-success add-book'>+</a></li>
    	}
	}

  handleSnackBarRequestClose = () => {
  	this.setState({snackBarOpen: false})
  }
	render() {
		const { author, filter } = this.props

		const style = {
			row: {
				borderBottom: '1px solid ' + grey400,
				padding: '0px 0px 2px 0px',
				margin: '2px 0 0px 0'
			},
			author: {
				height: '40px'
			},
			underlineStyle: {
				borderColor: '#FFF'
			},
			book: {
				margin: '5px 0 5px 0'
			},
			colContainer: {
				padding: '0 5px'
			}
		}

		return(
			<Row style={style.row}>
				<Col lg={2} md={3} sm={4} style={style.colContainer}> 
			    <TextField style={style.author} underlineStyle={style.underlineStyle} id={author.name} defaultValue={author.name} />
				</Col>
				<Col lg={10} md={9} sm={8} style={style.colContainer}>
			    	{author.books.filter((it) => {return !filter.enabled || filter.result.includes(it.toString())}).map((bookId) => {
			    		return <BookEntry key={bookId} id={bookId} removeBook={this.handleRemoveBook} />
			    	})}
				</Col>
        <Snackbar
          open={this.state.snackBarOpen}
          message='Book was deleted'
          autoHideDuration={4000}
          onRequestClose={this.handleSnackBarRequestClose}
        />
			</Row>
		)
	}
}
		 			// {this.renderAddBook()}

AuthorRow.propTypes = {
	author: PropTypes.object.isRequired,
	addBook: PropTypes.func.isRequired,
	filter: PropTypes.object.isRequired,
	removeBook: PropTypes.func.isRequired
}