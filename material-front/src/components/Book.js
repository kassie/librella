import React, { PropTypes, Component } from 'react'
import * as statuses from '../constants/BookStatuses'
import FlatButton from 'material-ui/FlatButton'
import Popover from 'material-ui/Popover'
import Menu from 'material-ui/Menu'
import MenuItem from 'material-ui/MenuItem'
import {green200, yellow200, red200, purple200} from 'material-ui/styles/colors'

const BOOK_STATUS_COLORS = {
	[statuses.DEFAULT]: '#FFF',
	[statuses.DONE]: green200,
	[statuses.ACTIVE]: yellow200,
	[statuses.REJECTED]: red200,
	[statuses.PENDING]: purple200
}

const BOOK_STATUS_BUTTONS = [
	{
		id: statuses.DONE,
		text: 'Done'
	} , {
		id: statuses.ACTIVE,
		text: 'In progress'
	} , {
		id: statuses.REJECTED,
		text: 'Rejected'
	} , {
		id: statuses.PENDING,
		text: 'Pending'
	} , {
		id: statuses.DEFAULT,
		text: 'Default'
	}
]

export default class Book extends Component  {
	state = {
		showConfirmIcons: false,
		open: false
	}

	handleRemoveConfirmClick = () => {
		this.props.removeBook(this.props.book.id)
		this.showSnackBar()
	}

	showSnackBar() {
		console.log('invoking remove callback')
		this.setState({snackBarOpen: true})
	}

  handleTouchTap = event => {
    this.setState({
      open: !this.state.open,
      anchorEl: event.currentTarget
    })
  }

  handlePopverCloseRequest = () => {
  	this.setState({open: false})
  }

	render() {
		const style = {
			book: {
				margin: '3px 5px 3px 0'
			},
			menu: {...BOOK_STATUS_COLORS},
			menuItem: {
				lineHeight: '32px'
			}
		}

		const { book, updateStatus } = this.props
		return(
			<span>
      	<FlatButton style={style.book} backgroundColor={BOOK_STATUS_COLORS[book.status]} label={book.title} onTouchTap={this.handleTouchTap} />
        <Popover
          open={this.state.open}
          anchorEl={this.state.anchorEl}
          anchorOrigin={{horizontal: 'left', vertical: 'bottom'}}
          targetOrigin={{horizontal: 'left', vertical: 'top'}}
          onRequestClose={this.handlePopverCloseRequest}
          useLayerForClickAway={false}>
          <Menu>
          	{BOOK_STATUS_BUTTONS.map((it) => {
	            return <MenuItem key={it.id} 
	            					style={{...style.menuItem, backgroundColor: BOOK_STATUS_COLORS[it.id]}} 
	            					onClick={() => { this.setState({open: false}); 
	            					updateStatus(book.id, it.id)}} 
	            					primaryText={it.text} 
          						/>
          	})}
	          <MenuItem style={style.menuItem} onClick={this.handleRemoveConfirmClick} primaryText='Remove' />
          </Menu>
        </Popover>
      </span>
		)
	}
}

Book.propTypes = {
	book: PropTypes.object.isRequired,
	updateStatus: PropTypes.func.isRequired,
	removeBook: PropTypes.func.isRequired
}

