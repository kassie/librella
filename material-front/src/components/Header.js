import React, {	Component } from 'react'
import SearchContainer from '../containers/SearchContainer'
import {Row, Col} from 'react-flexbox-grid/lib'
import {cyan500} from 'material-ui/styles/colors'

export default class Header extends Component {
	render() {
		const style = {
			container: {
				padding: '0 10px',
				backgroundColor: cyan500,
				position: 'relative',
				boxShadow: 'rgba(0, 0, 0, 0.117647) 0px 1px 6px, rgba(0, 0, 0, 0.117647) 0px 1px 4px'
			},
			search: {
				textAlign: 'right'
			}
		}
		return(
			<Row style={style.container}>
				<Col md={6} sm={6} xs={4}><h1>Librella</h1></Col>
				<Col md={6} sm={6} xs={8} style={style.search}>
	      		<SearchContainer />
				</Col>
			</Row>
		)
	}
}
			// <AppBar title='Librella'>
			// </AppBar>

Header.propTypes = {
}