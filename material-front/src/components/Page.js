import React, {	PropTypes, Component } from 'react'
import AuthorRowContainer from '../containers/AuthorRowContainer'
import {Tabs, Tab} from 'material-ui/Tabs'
import TextField from 'material-ui/TextField'
import FlatButton from 'material-ui/FlatButton'
import {green200} from 'material-ui/styles/colors'

export default class Page extends Component {
	state = {
		activeSection: this.props.library[0],
		addingAuthor: false,
		newAuthorName: '',
		showAddSection: false,
		newSectionName: ''
	}

	handleSwitchTab = section => {
		this.setState({activeSection: section})
	}

	handleAddAuthor = () => {
		this.setState({addingAuthor: true})
	}

	// handleCancelAddAuthor = () => {
	// 	this.hideNewAuthorInput()
	// }

	handleChangeNewAuthor = e => {
		this.setState({newAuthorName: e.target.value})
	}

	handleNewAuthorEnter = e => {
		if(e.key === 'Enter') {
			this.hideNewAuthorInput()	
			this.props.addAuthor(this.state.activeSection, this.state.newAuthorName)
		} else if (e.key === 'Escape') {
			this.hideNewAuthorInput()
		}
	}

	handleAddSectionInputBlur = () => {
		this.hideNewSectionControls()
	}

	handleAddAuthorInputBlur = () => {
		this.hideNewAuthorInput()
	}

	hideNewAuthorInput() {
		this.setState({addingAuthor: false, newAuthorName: ''})
	}

	renderAddAuthor() {
		const style = {
			margin: '5px 0 0 5px'
		}
		// console.log('render add author')
		if(this.state.addingAuthor) {
							// ref={ref => {if(ref !== null) ref.focus()}} 
			return (
				<div style={style}>
					<TextField 
							name='author-input'
							value={this.state.newAuthorName} 
							onChange={this.handleChangeNewAuthor} 
							onKeyDown={this.handleNewAuthorEnter} 
							onBlur={this.handleAddAuthorInputBlur}
						/>
				</div>
			)
					// <FlatButton backgroundColor={red200} onTouchTap={this.handleCancelAddAuthor} label='Cancel' />
		} else {
			return <FlatButton style={style} backgroundColor={green200} onTouchTap={this.handleAddAuthor} label='Add author' />
		}
	}

	handleNewSectionNameChange = e => {
		this.setState({newSectionName: e.target.value})
	}

	handleShowAddSection = () => {
		this.setState({showAddSection: true})
	}

	handleAddSection = e => {
		if(e.key === 'Enter' && this.state.newSectionName.length > 0) {
			this.props.addSection(this.state.newSectionName)
			this.hideNewSectionControls()
		} else if (e.key === 'Escape') {
			this.hideNewSectionControls()
		}
	}

	handleNewSectionCancelClick = () => {
		this.hideNewSectionControls()
	}

	hideNewSectionControls() {
		this.setState({showAddSection: false, newSectionName: ''})
	}

	renderAddSection() {
		let inner
		if(this.state.showAddSection) {
			inner = 
				<span className='new-section-input'>
					<input type='text' 
						onKeyDown={this.handleAddSection} 
						onBlur={this.handleAddSectionInputBlur}
						onChange={this.handleNewSectionNameChange}  
						ref={(it) => { if(it !== null) it.focus()} } 
						value={this.state.newSectionName} />
					<a href='#' onClick={this.handleNewSectionCancelClick} className='cancel-button tile tile-danger'>Cancel</a>
				</span>
		} else {
			inner = <a href='#' onClick={this.handleShowAddSection} >+</a>
		}

		return (
			<li className={`fade ${this.state.showAddSection ? 'in' : ''}`}>
				{inner}
			</li>
		)
	}

	render() {
		const { library, sections } = this.props

		return (
			<Tabs>
      	{library.map((sectionId, i) => {
      		const section = sections[sectionId]
        	return(
        		<Tab key={i} label={section.name}>
								{section.authors.map((authorId) => {
									return(
										<AuthorRowContainer key={authorId} id={authorId} />
									)
								})}
								{this.renderAddAuthor()}
        		</Tab>
      		)	 
      	})}
			</Tabs>
		)
	}
}

Page.propTypes = {
	sections: PropTypes.object.isRequired,
	authors: PropTypes.object.isRequired,
	library: PropTypes.array.isRequired,
	addBook: PropTypes.func.isRequired,
	addAuthor: PropTypes.func.isRequired,
	addSection: PropTypes.func.isRequired
}