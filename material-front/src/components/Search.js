import React, {	PropTypes, 	Component } from 'react'
import * as searchTypes from '../constants/SearchTypes'
import TextField from 'material-ui/TextField'

export default class Search extends Component {
	constructor(props) {
		super(props)
		this.state = {
			type: searchTypes.BOOK,
			query: ''
		}
		this.handleQueryChange = this.handleQueryChange.bind(this)
	}

	handleQueryChange(e) {
		this.setState({query: e.target.value})
		this.props.search(e.target.value, this.state.type)
	}

	render() {
		const placeholder = `Search ${this.state.type}s...`

		const style = {
			color: '#FFF'
		}

		return(
			<TextField
      			hintText={placeholder}
      			hintStyle={style}
      			inputStyle={style}
	      		onChange={this.handleQueryChange}
	      		value={this.state.query} 
	      	/>
		)
	}
}

Search.propTypes = {
	search: PropTypes.func.isRequired
}