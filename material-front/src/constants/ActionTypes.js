const RECEIVE_BOOKS = 'receive_books'
const UPDATE_STATUS = 'update_status'
const ADD_BOOK = 'add_book'
const ADD_AUTHOR = 'add_author'
const SEARCH = 'search'
const REMOVE_BOOK = 'remove_book'
const ADD_SECTION = 'add_section'

export { RECEIVE_BOOKS, UPDATE_STATUS, ADD_BOOK, ADD_AUTHOR, SEARCH, REMOVE_BOOK, ADD_SECTION }