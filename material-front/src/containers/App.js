import React, { Component} from 'react'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import Header from '../components/Header'
import Page from '../components/Page'
import { addBook, addAuthor, addSection} from '../actions/LibraryActions'
import getMuiTheme from 'material-ui/styles/getMuiTheme'
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider'
import { Grid } from 'react-flexbox-grid/lib'

class App extends Component {
	render() {
		const { authors, sections, library, addBook, addAuthor, addSection } = this.props
		return(
			<MuiThemeProvider muiTheme={getMuiTheme()}>
					<Grid style={{width: '100%'}}>
						<Header />
						<Page library={library} sections={sections} authors={authors} addBook={addBook} addAuthor={addAuthor} addSection={addSection} />
					</Grid>
			</MuiThemeProvider> 
		)
	}
}

function mapStateToProps(state) {
	return {
		...state
	}
}

function mapDispatchToProps(dispatch) {
	return {
		addBook: bindActionCreators(addBook, dispatch),
		addAuthor: bindActionCreators(addAuthor, dispatch),
		addSection: bindActionCreators(addSection, dispatch)
	}
}

export default connect(mapStateToProps, mapDispatchToProps)(App)