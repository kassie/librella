import { addBook, removeBook} from '../actions/LibraryActions'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import AuthorRow from '../components/AuthorRow'

function mapStateToProps(state, ownProps) {
	return {
		filter: state.filter,
		author: state.authors[ownProps.id]
	}
}

function mapDispatchToProps(dispatch) {
	return {
		addBook: bindActionCreators(addBook, dispatch),
		removeBook: bindActionCreators(removeBook, dispatch)
	}
}

export default connect(mapStateToProps, mapDispatchToProps)(AuthorRow)

