import { connect } from 'react-redux'
import Search from '../components/Search'
import { search } from '../actions/LibraryActions'
import { bindActionCreators } from 'redux'


function mapStateToProps() {
  return {}
}

function mapDispatchToProps(dispatch) {
  return {
    search: bindActionCreators(search, dispatch)
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Search)
