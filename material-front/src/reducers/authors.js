import * as types from '../constants/ActionTypes'

export default function authors(authors = {}, action) {
    switch(action.type) {
        case types.ADD_BOOK: {
            const {id, authorId } = action.payload
            return {
                ...authors, 
                [authorId]: {
                    ...authors[authorId],
                    ...{ books: [...authors[authorId].books, id] }
                }
            }
        }
        case types.ADD_AUTHOR: {
            const {id, name} = action.payload
            return {
                ...authors,
                [id]: {
                    id: id,
                    name: name,
                    books: []
                }
            }
        }
        case types.REMOVE_BOOK: {
            const { authorId, bookId } = action.payload
            return {
                ...authors,
                [authorId]: {
                    ...authors[authorId],
                    books: authors[authorId].books.filter((it) => { return it != bookId })
                }
            }
        }
        default: {
        	if(action.payload && action.payload.entities && action.payload.entities.authors) {
        		return {
        			...{},
        			...authors,
        			...action.payload.entities.authors
        		}
        	}
            return authors
        }
    }
}