import * as types from '../constants/ActionTypes'
import { DEFAULT } from '../constants/BookStatuses'

export default function books(books = {}, action) {
    switch(action.type) {
        case types.RECEIVE_BOOKS:
            return {
                ...books,
                ...action.payload.entities.books
            }
        case types.ADD_BOOK:
            return {
                ...books, 
                [action.payload.id]: {
                    id: action.payload.id, 
                    name: action.payload.name,
                    status: DEFAULT
                }
            }
        case types.UPDATE_STATUS: {
            const { book, status } = action.payload   
            return {
                ...books,
                [book]: {
                    ...books[book],
                    status: status
                }
            }
        }
        case types.REMOVE_BOOK: {
            const { bookId } = action.payload
            const updated = {...{}, ...books}
            delete updated[bookId]
            return updated
        }
        default: {
        	if(action.payload && action.payload.entities && action.payload.entities.books) {
        		return {
        			...{},
        			...books,
        			...action.payload.entities.books
        		}
        	}
            return books
        }
    }
}