/*eslint-disable */
export const ping = store => next => action => {
  console.log(`Action ${action.type}, data: ${action.payload}`)
  return next(action)
}
/*eslint-enable */